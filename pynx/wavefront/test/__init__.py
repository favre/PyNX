#! /usr/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2024present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""
This package includes tests for the wavefront python API.
"""

import unittest

from .test_wavefront import suite as test_wavefront_suite


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(test_wavefront_suite())
    return test_suite
