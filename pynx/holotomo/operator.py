# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2017-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

from ..processing_unit import has_cuda, has_opencl, Backend
from ..processing_unit import default_processing_unit as main_default_processing_unit
from .cpu_operator import *

# Import CUDA operators if possible, otherwise OpenCL
if has_cuda and main_default_processing_unit.backend not in [Backend.PYOPENCL, Backend.CPU]:
    from .cu_operator import *
elif has_opencl and main_default_processing_unit.backend not in [Backend.PYCUDA, Backend.CPU]:
    raise NotImplementedError("Holotomo OpenCL operators are not available")
    from .cl_operator import *
