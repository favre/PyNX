#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2023-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr

"""
This package includes tests for the holotomo python API.
"""

import unittest

from .test_holotomo import suite as test_holotomo_suite


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTest(test_holotomo_suite())
    return test_suite
