
/** Compute the scale factor between calculated and observed intensities. Negative observed intensities are
* treated as masked and ignored. The reduced scale factor should be multiplied by the calculated intensities to match
* observed ones.
*/

__device__ my_float4 scale_intensity(const int i, float* obs, complexf *calc, float* background,
                                     float* cx, const int nxy, const int nxystack, const int nb_mode)
{
  const float iobs = obs[i];
  if(iobs < 0) return my_float4(0);

  float icalc = 0;
  for(int imode=0;imode<nb_mode;imode++) icalc += dot(calc[i + imode * nxystack], calc[i + imode * nxystack]);

  const int iframe = i / nxy;
  // cx>1e8 indicate a frame with no sample (direct beam), used to scale separately the probe
  if(cx[iframe]>1e8) return my_float4(0, 0, iobs - background[i % nxy], icalc);
  return my_float4(iobs - background[i % nxy], icalc, 0, 0);
}
