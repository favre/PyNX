#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import sys
import os
from pynx.ptycho.runner import PtychoRunnerException
from pynx.ptycho.runner.p06 import PtychoRunnerP06, PtychoRunnerScanP06


def main():
    try:
        w = PtychoRunnerP06(sys.argv, None, PtychoRunnerScanP06)
        w.process_scans()
    except PtychoRunnerException as ex:
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        print(f"\nFor the command-line help, use:\n   {os.path.split(sys.argv[0])[-1]} --help ")
        sys.exit(1)


if __name__ == '__main__':
    main()
