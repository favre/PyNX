#! /opt/local/bin/python
# -*- coding: utf-8 -*-

# PyNX - Python tools for Nano-structures Crystallography
#   (c) 2016-present : ESRF-European Synchrotron Radiation Facility
#       authors:
#         Vincent Favre-Nicolin, favre@esrf.fr


import sys
from pynx.ptycho.runner import helptext_generic, PtychoRunnerException
from pynx.ptycho.runner.tps25a import PtychoRunnerTPS25A, PtychoRunnerScanTPS25A, helptext_beamline, params

help_text = helptext_generic + helptext_beamline


def main():
    try:
        w = PtychoRunnerTPS25A(sys.argv, params, PtychoRunnerScanTPS25A)
        w.process_scans()
    except PtychoRunnerException as ex:
        print(help_text)
        print('\n\n Caught exception: %s    \n' % (str(ex)))
        sys.exit(1)
    if "pynx-tps25apty.py" in sys.argv[0]:
        print("DEPRECATION warning: please use 'pynx-ptycho-tps25a' instead of 'pynx-tps25apty.py'")


if __name__ == '__main__':
    main()
