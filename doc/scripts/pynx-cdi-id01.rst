pynx-cdi-id01
=============
Documentation for the `pynx-cdi-id01` command-line script to
reconstruct objects from a CDI dataset, with defaults parameters
for the Bragg CDI case.

.. argparse::
   :module: pynx.cdi.runner.id01
   :func: make_parser_sphinx
   :prog: pynx-cdi-id01
