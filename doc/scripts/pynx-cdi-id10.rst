pynx-cdi-id10
-------------
Documentation for the `pynx-cdi-id10` command-line script to
reconstruct objects from a CDI dataset, with defaults parameters
for the small-angle CDI case.

.. argparse::
   :module: pynx.cdi.runner.id10
   :func: make_parser_sphinx
   :prog: pynx-cdi-id10
