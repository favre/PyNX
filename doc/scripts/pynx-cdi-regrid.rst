pynx-cdi-regrid
---------------
Instructions to run the `pynx-cdi-regrid` command-line script to prepare
a 3D cdi CXI file from multiple projections.

.. argparse::
   :module: pynx.cdi.runner.regrid
   :func: make_parser
   :prog: pynx-cdi-regrid
